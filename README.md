# Multi-User Adventures (MUA)

This is my take on a MUD system, written in Rust, that implements:

- A scripting language to write the rooms and puzzles in (similar to LPC).
- A telnet interface.
- A complex parser.
- Supporting systems (inventory, combat, crafting etc).

## Architecture

The first system (interface module) managed connections, takes input, writes
output and converts input into a structure for the main game to use.  This
structure will contain a player ID, command and the command's data.

Examples of commands could be:

- Player logged in.
- New player logged in.
- Player logged out.
- Player typed command.

The commands are fed via a MPSC channel to the core module, which is essentially
the MUD engine.  It passes the command to the scripting language engine, which
processes it and posts output commands to the interface module.  This could be a
single command to a player, multiple commands to all the players in a room, or
multiple commands to all players on the server.  It's up to the script.

## Scripting language

This is a dynamic prototype object-oriented language as objects suit MUDs very
well, e.g. rooms, items, players etc.  Each object sits in a folder hierarchy.
This folder hierarchy is built from the folder structure of the scripts and
created dynamically at runtime.  Objects are created by cloning a prototype into
another folder.  Each object is given an identifier.  Objects do not have types,
and act similar to JavaScript objects.

The scripting language will be very lisp-like and can define macros.

## Debug System

The interface module allows someone to login as a debugger.  From here, there
are DOS-like commands to navigate the object hierarchy and probe it.  This is a
special user.